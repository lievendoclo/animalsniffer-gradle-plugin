package be.insaneprogramming.gradle.animalsniffer

import groovy.transform.Canonical
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.InvalidUserDataException
import org.gradle.api.Plugin
import org.gradle.api.Project

import org.codehaus.mojo.animal_sniffer.ClassListBuilder
import org.codehaus.mojo.animal_sniffer.SignatureChecker
import org.codehaus.mojo.animal_sniffer.logging.Logger
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskAction
import org.slf4j.LoggerFactory

class AnimalSnifferPlugin implements Plugin<Project> {
    private org.slf4j.Logger logger = LoggerFactory.getLogger(this.class)

    @Override
    void apply(Project project) {
        project.configurations.maybeCreate("signature")
        project.extensions.create("animalsniffer", AnimalSnifferExtension)
        project.afterEvaluate {
            AnimalSnifferExtension extension = project.animalsniffer
            if (!extension.skip) {
                if (!(extension.signature || extension.signatures)) {
                    throw new InvalidUserDataException("One or more signatures are required when using Animal Sniffer plugin")
                } else {
                    if(extension.signature && extension.signatures) {
                        throw new InvalidUserDataException("Use either signature or signatures, not both")
                    }
                    if(extension.signature) {
                        project.dependencies {
                                signature extension.signature
                        }
                    } else {
                        project.dependencies {
                            extension.signatures.each {
                                signature it
                            }
                        }
                    }
                }
                MainAnimalSnifferTask task = project.tasks.create(MainAnimalSnifferTask.TASK_NAME, MainAnimalSnifferTask)
                task.sourceSet = "main"
                task.dependsOn(JavaPlugin.COMPILE_JAVA_TASK_NAME)
                project.tasks.findByName(JavaPlugin.CLASSES_TASK_NAME).dependsOn(MainAnimalSnifferTask.TASK_NAME)

                if (extension.runForTests) {
                    TestAnimalSnifferTask testTask = project.tasks.create(TestAnimalSnifferTask.TEST_TASK_NAME, TestAnimalSnifferTask)
                    testTask.sourceSet = "test"
                    testTask.dependsOn(JavaPlugin.COMPILE_TEST_JAVA_TASK_NAME)
                    project.tasks.findByName(JavaPlugin.TEST_CLASSES_TASK_NAME).dependsOn(TestAnimalSnifferTask.TEST_TASK_NAME)
                }
            }
        }
    }


}

class MainAnimalSnifferTask extends AnimalSnifferTask {
    public static final String TASK_NAME = "animalSniffer"

    MainAnimalSnifferTask() {
        sourceSet = "main"
    }
}

class TestAnimalSnifferTask extends AnimalSnifferTask {
    public static final String TEST_TASK_NAME = "testAnimalSniffer"

    TestAnimalSnifferTask() {
        sourceSet = "test"
    }
}

abstract class AnimalSnifferTask extends DefaultTask {
    String sourceSet

    @TaskAction
    def performAnimalSniffer() {
        AnimalSnifferExtension extension = project.animalsniffer
        def logger = new GradleLogger(logger)
        def signatures = project.configurations.signature.resolvedConfiguration.resolvedArtifacts*.file
        ClassListBuilder plb = new ClassListBuilder(logger);
        plb.process(project.buildDir);
        if (extension.excludeDependencies) {
            def files = project.configurations.runtime.resolvedConfiguration.resolvedArtifacts*.file
            files.each {
                plb.process(it)
            }
        }
        def ignored = plb.getPackages();
        if (extension.ignores) {
            extension.ignores.each {
                if (it) {
                    ignored.add(it.replace('.', '/'))
                }
            }
        }
        signatures.each {
            def checker = new SignatureChecker(it.newInputStream(), ignored, logger)
            if (extension.annotations) {
                checker.setAnnotationTypes(extension.annotations)
            }
            def sourceSet = project.sourceSets[sourceSet] as SourceSet
            def allSources = sourceSet.allJava.srcDirs
            checker.sourcePath = allSources as List
            if (sourceSet.output.classesDir)
                checker?.process(sourceSet.output.classesDir)
            if (checker.signatureBroken)
                throw new GradleException("Signature errors found. Verify them and ignore them with the proper annotation if needed.")
        }
    }
}


@Canonical
class GradleLogger implements Logger {
    @Delegate
    org.slf4j.Logger logger
}

class AnimalSnifferExtension {
    boolean excludeDependencies = true
    String signature = ""
    String[] signatures = []
    def ignores = []
    def annotations = []
    boolean skip = false
    boolean runForTests = false
}

